import React,{useState} from 'react';
import { IoIosNotificationsOutline } from "react-icons/io";
import TimePicker from 'react-time-picker';

function Header1() {
    const [value, onChange] = useState('10:00');
    return (
        <div style={{marginLeft:"200px"}}>
        <TimePicker
          onChange={onChange}
          value={value}
        />
      </div>
    )
}

export default Header1
