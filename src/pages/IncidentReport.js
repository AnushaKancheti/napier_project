// const Home = () => {
//   return <div>aaaa</div>;
// };
// export default Home;

import React from "react";
// import Search1 from "./Search";
import { BiSearch } from "react-icons/bi";
import Dropdown2 from "./Dropdown2";
// import { Button } from "antd";
import Header1 from '../components/Header1'
function IncidentReport() {
  return (
    <div
      className=""
      style={{ backgroundColor: "white", height: "100%", width: "100%" }}
    >
      <div className="Report">REPORT AN INCIDENT</div>
      <div style={{ marginTop: "20px" }}>
        <h4 className="heading-1 ">
          <span>INCIDENT DETAILS</span>
        </h4>
      </div>

      {/* <div className="row">
        <div className="col-md-3">
          <h5>Incident Date & Time</h5>
          <div>
            <input
              type="date"
              id="accident"
              name="accident"
              style={{ fontSize: "14px" }}
            />
          </div>
        </div>
      </div> */}

      <div className="row ">
        <div className="col-md-3">
          <h5 style={{ fontSize: "12px", marginLeft: "220px", width: "60%" }}>
            Incident Date & Time
          </h5>
          <div style={{ marginTop: "-28px" }}>
            <input
              type="date"
              id="accident"
              name="accident"
              style={{ fontSize: "14px", marginLeft: "215px", width: "55%" }}
            />
          </div>
        </div>
      

        <div className="col-md-4" style={{ marginLeft: "75px" }}>
          <h5 style={{ fontSize: "12px", marginLeft: "20px" }}>
            Location of incident
          </h5>
          <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Nursing Station"
              name="search"
              style={{ fontSize: "12px", marginLeft: "20px", width: "60%" }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "-25px",
                marginTop: "15px",
                color: "gray",
              }}
            />
          </div>
        </div>

        <div className="col-md-4">
          <h5 style={{ fontSize: "12px", marginLeft: "-150px" }}>
            Location Details if any
          </h5>
          <div style={{ marginTop: "-28px" }}>
            <input
              type="text"
              id="locationDetails"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px", marginLeft: "-150px", width: "60%" }}
            />
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-3">
          <h5 style={{ fontSize: "12px", marginLeft: "220px" }}>YES/NO</h5>
         {/* <div> <label className="switch">
          <input type="checkbox"/>
          <div className="slider" style={{marginLeft:"210px"}}> </div>
         </label>
         </div> */}
        </div>

        <div className="col-md-3" style={{ marginLeft: "75px" }}>
          <h5 style={{ fontSize: "12px", marginLeft: "18px" }}>
            Patient Name / UHID
          </h5>
          <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px", marginLeft: "18px", width: "83%" }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "-25px",
                marginTop: "15px",
                color: "gray",
              }}
            />
          </div>
        </div>

        <div className="col-md-3">
          <h5 style={{ fontSize: "12px", marginLeft: "-35px" }}>
            Compalint Date & Time
          </h5>
          <div style={{ marginTop: "-28px" }}>
            <input
              type="date"
              id="accident"
              name="accident"
              style={{ fontSize: "12px", marginLeft: "-35px", width: "83%" }}
            />
          </div>
        </div>

        <div className="col-md-2">
          <h5 style={{ fontSize: "12px", marginLeft: "-100px" }}>
            Complaint ID
          </h5>
          <div style={{ marginTop: "-28px" }}>
            <input
              type="text"
              id="locationDetails"
              placeholder="Compaint ID"
              name="search"
              style={{ fontSize: "12px", marginLeft: "-100px", width: "120%" }}
            />
          </div>
        </div>
      </div>
      <div style={{ marginTop: "20px" }}>
        <h4 class="heading-1">
          <span>TYPE OF INCIDENT</span>
        </h4>
      </div>

      <div className="row">
        <div
          className="col-md-12"
          style={{ marginLeft: "222px", marginTop: "-22px" }}
        >
          <input
            id="unsafe"
            value="unsafe"
            name="platform"
            type="radio"
            style={{ marginLeft: "15px" }}
          />
          <span style={{ fontSize: "12px", marginLeft: "4px" }}>
            Unsafe Condition
          </span>
          <input
            id="NoHarm"
            value="NoHarm"
            name="platform"
            type="radio"
            style={{ marginLeft: "15px" }}
          />
          <span style={{ fontSize: "12px", marginLeft: "16x" }}>No Harm</span>
          <input
            id="newvermiss"
            value="newvermiss"
            name="platform"
            type="radio"
            style={{ marginLeft: "15px" }}
          />
          <span style={{ fontSize: "12px", marginLeft: "4px" }}>
            Never Miss
          </span>

          <input
            style={{ marginLeft: "15px" }}
            id="adverseevent"
            value="adverseevent"
            name="platform"
            type="radio"
            checked="true"
          />
          <span style={{ fontSize: "12px", marginLeft: "4px" }}>
            Adverse Event
          </span>

          <input
            style={{ marginLeft: "15px" }}
            id="sentinelEvent"
            value="sentinelEvent"
            name="platform"
            type="radio"
          />
          <span style={{ fontSize: "12px", marginLeft: "4px" }}>
            Sentinel Event
          </span>
        </div>
      </div>

      <div className="row">
        <div className="col-md-12" style={{ marginLeft: "222px" }}>
          <table style={{ width: "77%" }}>
            <tr
              style={{
                display: "block",
                background: "#3F5083",
                borderBottom: "1px solid",
              }}
            >
              <th style={{ fontSize: "14px", color: "white" ,padding: "6px"}}>
                {" "}
                ADVERSE EVENT{" "}
              </th>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-25px" }}>
                  <span
                    style={{
                      fontSize: "12px",
                      marginTop: "30px",
                      marginLeft: "20px",
                    }}
                  >
                    Degree of Harm to Patient/ Resident
                  </span>

                  <div class="column" style={{ marginLeft: "60px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="Dream"
                      value="Dream"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      Dream
                    </span>
                  </div>
                  <div class="column" style={{ marginLeft: "50px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="SevereHorm"
                      value="SevereHorm"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      Severe Harm
                    </span>
                  </div>
                  <div class="column" style={{ marginLeft: "50px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="modarateHarm"
                      value="modarateHarm"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      Moderate Harm
                    </span>
                  </div>

                  <div class="column" style={{ marginLeft: "50px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="mildHarm"
                      value="mildHarm"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      Mild Harm
                    </span>
                  </div>
                  <div class="column">
                    <input
                      style={{
                        height: "27px",
                        width: "184px",
                        marginLeft: "36px",
                        fontSize: "12px",
                      }}
                      id="Scratchonskin"
                      value="Scratch on skin"
                      name="platform"
                      type="text"
                    />
                  </div>
                </div>
              </td>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-10px" }}>
                  <div className="column">
                    <span
                      style={{
                        fontSize: "12px",
                        marginTop: "30px",
                        marginLeft: "20px",
                      }}
                    >
                      Duration of harm to the patient/residernt
                    </span>
                  </div>

                  <div
                    className="column"
                    style={{ marginLeft: "-116px", marginTop: "12px" }}
                  >
                    <div class="row col-md-12" style={{ marginLeft: "143px" }}>
                      <input
                        style={{ marginLeft: "15px" }}
                        id="Dream"
                        value="Dream"
                        name="platform"
                        type="radio"
                      />
                      <span
                        style={{
                          fontSize: "12px",
                          marginLeft: "4px",
                          color: "gray",
                        }}
                      >
                        Permanent:Note expected to revert to
                        approximatelynormal(i.e., patient's baseline)
                      </span>
                    </div>
                    <div class="row col-md-12" style={{ marginLeft: "143px" }}>
                      <input
                        style={{ marginLeft: "15px" }}
                        id="SevereHorm"
                        value="SevereHorm"
                        name="platform"
                        type="radio"
                      />
                      <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                        Temporart:Expected to revert to approximately normal
                        (i.e., patient's baseline)
                      </span>
                    </div>
                  </div>
                </div>
              </td>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-25px" }}>
                  <span
                    style={{
                      fontSize: "12px",
                      marginTop: "30px",
                      marginLeft: "20px",
                    }}
                  >
                    Notification of patient,family or guardian
                  </span>

                  <div class="column">
                    <input
                      style={{ marginLeft: "60px" }}
                      id="Dream"
                      value="Dream"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      Was Notified
                    </span>
                  </div>

                  <div class="column" style={{ marginLeft: "20px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="modarateHarm"
                      value="modarateHarm"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      Not notified
                    </span>
                  </div>
                </div>
              </td>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-25px" }}>
                  <span
                    style={{
                      fontSize: "12px",
                      marginTop: "30px",
                      marginLeft: "20px",
                    }}
                  >
                    increased length of stay expected due to incident
                  </span>

                  <div class="column">
                    <input
                      style={{ marginLeft: "15px" }}
                      id="Dream"
                      value="Dream"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "3px" }}>
                      Yes
                    </span>
                  </div>
                  <div class="column" style={{ marginLeft: "65px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="SevereHorm"
                      value="SevereHorm"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "4px" }}>
                      No
                    </span>
                  </div>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <div style={{ marginTop: "20px" }}>
        <h4 class="heading-1">
          <span> INCIDENT CATEGORY</span>
        </h4>
      </div>

      <div className="row">
        <div className="col-md-3"
          style={{
            marginLeft: "237px",
            fontSize: "12px",
            marginTop: "10px",
            fontWeight: "bold",
          }}
        >
          Incident Category
          <div style={{width:"210px",fontWeight: "100"}}> <Dropdown2/></div>
         
        </div>
        <div className="col-md-3"
          style={{
            marginLeft: "-100px",
            fontSize: "12px",
            marginTop: "10px",
            fontWeight: "bold",
          }}
        >
          Incident Sub-Category
          <div style={{width:"210px",fontWeight: "100"}}> <Dropdown2/></div>
         
        </div>
      
      
      </div>
      <div style={{ marginTop: "20px" }}>
        <h4 class="heading-1">
          <span> INCIDENT DECSCRIPTION</span>
        </h4>
      </div>
      <h4 style={{ marginLeft: "225px" }}>Incident details</h4>
      <div className="row">
        <div className="col-md-12" style={{ marginLeft: "222px" }}>
          <table style={{ width: "77%", height: "77px" }}>
            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-10px" }}>
                  <div className="column">
                    <div
                      style={{
                        fontSize: "12px",
                        marginTop: "-16px",
                        marginLeft: "25px",
                      }}
                    >
                      Wrong dose administered due to oversight.The packet
                      dispenesed from pharamancy had the correct dose but the
                      strip was on the wrong dose.verficied the packet and
                      administered the drug
                    </div>
                  </div>

                  <div
                    className="column"
                    style={{ marginLeft: "-116px", marginTop: "12px" }}
                  >
                    <div
                      class="row col-md-12"
                      style={{ marginLeft: "143px" }}
                    ></div>
                  </div>
                </div>
              </td>
            </tr>
          </table>
        </div>
        <div class="row">
          <div
            className="col-md-3"
            style={{
              marginLeft: "237px",
              fontSize: "12px",
              marginTop: "10px",
              fontWeight: "bold",
            }}
          >
            Department Invovled
          </div>
          <div className="col-md-3" style={{ marginLeft: "75px" }}>
            <div
              style={{
                marginTop: "-17px",
                marginLeft: "-120px",
                width: "293px",
              }}
            >
              <input
                type="search"
                id="search"
                placeholder="search"
                name="search"
                style={{ fontSize: "12px", marginLeft: "18px", width: "83%" }}
              />
              <BiSearch
                style={{
                  fontSize: "20px",
                  marginLeft: "-25px",
                  marginTop: "15px",
                  color: "gray",
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12" style={{ marginLeft: "222px" }}>
          <table style={{ width: "77%" }}>
            <tr
              style={{
                display: "block",
                background: "#3F5083",
                borderBottom: "1px solid",
              }}
            >
              <th style={{ fontSize: "14px", color: "white",padding: "6px" }}>
                {" "}
                Preventability of Incident
              </th>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-25px" }}>
                  <div class="column" style={{ marginLeft: "8px" }}>
                    <input
                      style={{ marginLeft: "15px" }}
                      id="Dream"
                      value="Dream"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "10px" }}>
                      Likely could have been prevented
                    </span>
                  </div>
                </div>
              </td>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-25px" }}>
                  <div class="column">
                    <input
                      style={{ marginLeft: "24px" }}
                      id="Dream"
                      value="Dream"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "10px" }}>
                      Likely could not have been prevented
                    </span>
                  </div>
                </div>
              </td>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div
                  class="row"
                  style={{ marginTop: "-25px", marginLeft: "-5px" }}
                >
                  <div class="column">
                    <input
                      style={{ marginLeft: "15px" }}
                      id="Dream"
                      value="Dream"
                      name="platform"
                      type="radio"
                    />
                    <span style={{ fontSize: "12px", marginLeft: "10px" }}>
                      Not assessed
                    </span>
                  </div>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
      
      <div className="row" style={{marginTop:"20px"}}>
      <div style={{ marginLeft: "235px",fontSize:"12px",fontWeight:"bold" }}>Immediate Action taken</div>
        <div className="col-md-12" style={{ marginLeft: "222px" }}>
          <table style={{ width: "77%" }}>
            <tr
              style={{
                display: "block",
                background: "#3F5083",
                borderBottom: "1px solid",
              }}
            >
              <th style={{ fontSize: "14px", color: "white",padding: "6px" }}>
                {" "}
                Action taken
              </th>
              <div style={{marginLeft:"480px",marginTop:"-18px"}}>
              <th style={{ fontSize: "14px", color: "white"}}>
                {" "}
                Action Taken By
              </th>
              </div>
              <div style={{marginLeft:"730px",marginTop:"-20px"}}>
              <th style={{ fontSize: "14px", color: "white" }}>
                {" "}
                Date & Time 
              </th>
              </div>
              <div style={{marginLeft:"961px",marginTop:"-20px"}}>
              <th style={{ fontSize: "14px", color: "white" }}>
                {" "}
                Action 
              </th>
              </div>
            </tr>
            

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
              <div className="row ">
              <div className="col-md-4">
          
          <div style={{ marginTop: "-28px" }}>
            <input
              type="text"
              id="locationDetails"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px",
              marginLeft: "16px",
              width: "133%",
              marginTop: "34px",
              padding: "2px"}}
            />
          </div>
        </div>
     
        <div className="col-md-4" style={{ marginLeft: "75px" }}>
          
          <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px", marginLeft: "55px", width: "70%", marginTop: "34px",
              padding: "2px" }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "-25px",
                marginTop: "15px",
                color: "gray",
              }}
            />
          </div>
        </div>

        <div className="col-md-3">
          
          <div style={{ marginTop: "-28px" }}>
            <input
              type="date"
              id="accident"
              name="accident"
              style={{ fontSize: "12px", marginLeft: "-53px", width: "80%", marginTop: "34px",
              padding: "2px" }}
            />
          </div>
        </div>


       
      </div>

              </td>
            </tr>

            <tr
              style={{
                width: "1000px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-16px" }}>
                  <div class="column">
                    
                    <div style={{ fontSize: "12px",marginLeft: "31px",marginTop:"25px",padding: "5px"  }}>
                    Patient monitered for one hour,vitals were stable and informedp physician.
                    </div>
                    <div style={{ fontSize: "12px", marginLeft: "506px",marginTop:"-30px",padding: "5px" }}>
                 
                    Robert
                    </div>
                    <div style={{ fontSize: "12px", marginLeft: "750px",marginTop:"-30px",padding: "5px" }}>
                 
                12/12/2021</div>
                  </div>
                </div>
              </td>
            </tr>

          </table>
        </div>
      </div>
        
      <div className="row" style={{marginTop:"20px",height:"30px",width:"120%"}}>
      <div style={{ marginLeft: "235px",fontSize:"12px",fontWeight:"bold" }}>Incident witnesses by</div>
        <div className="col-md-12" style={{ marginLeft: "222px" }}>
          <table style={{ width: "30%" }}>
            <tr
              style={{
                display: "block",
                background: "#3F5083",
                borderBottom: "1px solid",
              }}
            ><div style={{marginLeft:"10px"}}>
              <th style={{ fontSize: "14px", color: "white" }}>
                {" "}
                Name
              </th>
              </div>
              <div style={{marginLeft:"220px",marginTop:"-18px"}}>
              <th style={{ fontSize: "14px", color: "white",marginTop:"-400px"}}>
                {" "}
                Department
              </th>
              </div>
              <div style={{marginLeft:"440px",marginTop:"-18px"}}>
              <th style={{ fontSize: "14px", color: "white",marginTop:"-400px",padding:"5px"}}>
                {" "}
                Action
              </th>
              </div>
             
             
            </tr>
            

            <tr
              style={{
                width: "200px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
              <div className="row" style={{height:"40px"}}>
              <div className="col-md-3">
          
              <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Search"
              name="search"
              style={{ fontSize: "12px", marginLeft: "12px", width: "180%", marginTop: "34px",
              }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "170px",
                marginTop: "-107px",
                color: "gray",
              }}
            />
          </div>
        </div>
     
        <div className="col-md-3" style={{ marginLeft: "75px" }}>
          
        <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px", marginLeft: "7px", width: "180%", marginTop: "34px",
              padding: "2px" }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "165px",
                marginTop: "-107px",
                color: "gray",
              }}
            />
          </div>
        </div>

       


       
      </div>

              </td>
            </tr>

            <tr
              style={{
                width: "200px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-16px" }}>
                  <div class="column">
                    
                    <div style={{ fontSize: "12px",marginLeft: "31px",marginTop:"20px",padding: "5px"  }}>
                    Jossi
                    </div>
                    <div style={{ fontSize: "12px", marginLeft: "225px",marginTop:"-30px",padding: "3px" }}>
                 
                    Nursing
                    </div>
                   
                  </div>
                </div>
              </td>
            </tr>

          </table>
        </div>
      </div>
      <div className="row" style={{marginTop:"-30px",height:"30px",marginLeft:"510px",width:"130%"}}>
      <div style={{ marginLeft: "235px",fontSize:"12px",fontWeight:"bold" }}>Incident witnesses by</div>
        <div className="col-md-12" style={{ marginLeft: "222px" }}>
          <table style={{ width: "30%" }}>
            <tr
              style={{
                display: "block",
                background: "#3F5083",
                borderBottom: "1px solid",
              }}
            ><div style={{marginLeft:"10px"}}>
              <th style={{ fontSize: "14px", color: "white" }}>
                {" "}
                Name
              </th>
              </div>
              <div style={{marginLeft:"160px",marginTop:"-18px"}}>
              <th style={{ fontSize: "14px", color: "white",marginTop:"-400px"}}>
                {" "}
                Department
              </th>
              </div>
              <div style={{marginLeft:"295px",marginTop:"-25px"}}>
              <th style={{ fontSize: "14px", color: "white",marginTop:"-400px",padding:"5px"}}>
                {" "}
                Date & Time
              </th>
              </div>
              <div style={{marginLeft:"440px",marginTop:"-27px"}}>
              <th style={{ fontSize: "14px", color: "white",marginTop:"-400px",padding:"5px"}}>
                {" "}
                Action
              </th>
              </div>
             
             
            </tr>
            

            <tr
              style={{
                width: "200px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
              <div className="row" style={{height:"40px"}}>
              <div className="col-md-3">
          
              <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Search"
              name="search"
              style={{ fontSize: "12px", marginLeft: "12px", width: "115%", marginTop: "34px",
              }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "115px",
                marginTop: "-107px",
                color: "gray",
              }}
            />
          </div>
        </div>
     
        <div className="col-md-3" style={{ marginLeft: "75px" }}>
          
        <div style={{ marginTop: "-28px" }}>
            <input
              type="search"
              id="search"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px", marginLeft: "-55px", width: "115%", marginTop: "33px",
              padding: "2px" }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "50px",
                marginTop: "-107px",
                color: "gray",
              }}
            />
          </div>
        </div>
        <div className="col-md-3">
          
          <div style={{ marginTop: "-28px" }}>
            <input
              type="date"
              id="accident"
              name="accident"
              style={{ fontSize: "12px", marginLeft: "-53px", width: "115%", marginTop: "31px",
              padding: "2px" }}
            />
          </div>
        </div>

       


       
      </div>

              </td>
            </tr>

            <tr
              style={{
                width: "200px",
                borderBottom: "1px solid gray",
                borderLeft: "1px solid gray",
                borderRight: "1px solid gray",
                borderTop: "1px solid gray",
              }}
            >
              <td>
                <div class="row" style={{ marginTop: "-16px" }}>
                  <div class="column">
                    
                    <div style={{ fontSize: "12px",marginLeft: "31px",marginTop:"20px",padding: "5px"  }}>
                    Hellena
                    </div>
                    <div style={{ fontSize: "12px", marginLeft: "170px",marginTop:"-30px",padding: "3px" }}>
                 
                    Nursing
                    </div>
                    <div style={{ fontSize: "12px", marginLeft: "310px",marginTop:"-30px",padding: "5px" }}>
                 
                 12/12/2021</div>
                   
                  </div>
                </div>
              </td>
            </tr>

          </table>
        </div>
      </div>
        
      <div className="row" style={{marginTop:"110px"}}>
        <div className="col-md-3">
          <div style={{ fontSize: "12px", marginLeft: "220px", width: "60%" }}>
            Upload
          </div>
          <div style={{ marginTop: "-28px",width:"370px" }}>
            <input
                type="text"
                id="locationDetails"
                placeholder="Item selection"
                name="search"
              style={{ fontSize: "12px", marginLeft: "222px", width: "60%" }}
            />
          </div>
        </div>

        <div className="col-md-3" style={{ marginLeft: "75px" }}>
          <div style={{ fontSize: "12px", marginLeft: "45px" }}>
            Incident Reorted by
          </div>
          <div style={{ marginTop: "-28px",width:"400px"  }}>
            <input
               type="text"
               id="locationDetails"
               placeholder="Robert"
               name="search"
              style={{ fontSize: "12px", marginLeft: "45px", width: "60%" }}
            />
           
          </div>
        </div>

        <div className="col-md-3">
          <div style={{ fontSize: "12px", marginLeft: "-11px" }}>
            Department
          </div>
          <div style={{ marginTop: "-28px",width:"370px"  }}>
            <input
              type="text"
              id="locationDetails"
              placeholder="Nursing"
              name="search"
              style={{ fontSize: "12px", marginLeft: "-10px", width: "60%" }}
            />
          </div>
        </div>
        <div className="col-md-3" style={{marginLeft: "990px",marginTop: "-50px" }}>
          <div style={{ fontSize: "12px", marginLeft: "20px" }}>
            Heade of Deportment
          </div>
          <div style={{ marginTop: "-28px",width:"400px" }}>
            <input
              type="search"
              id="search"
              placeholder="Enter"
              name="search"
              style={{ fontSize: "12px", marginLeft: "20px", width: "60%" }}
            />
            <BiSearch
              style={{
                fontSize: "20px",
                marginLeft: "-25px",
                marginTop: "15px",
                color: "gray",
              }}
            />
          </div>
        </div>
      </div>
      <div className="row">
        
      <div className="col-md-4" style={{marginLeft:"615px"}}> 
      <button type="button"  style={{height: "33px",width: "25%",fontSize:"20px"}} class="btn btn-outline-dark">Save</button>
      </div>
      <div className="col-md-4" style={{marginLeft: "745px",marginTop:"-60px"}}> 
        <button type="button" class="btn btn-success" style={{height: "33px",width: "25%",fontSize:"20px"}}>Submit</button></div>
      </div>
      
    </div>
  );
}

export default IncidentReport;
