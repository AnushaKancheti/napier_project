import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import { GiHamburgerMenu } from "react-icons/gi";
import "./App.css";
import Login from "./pages/Login";
import IncidentReport from "./pages/IncidentReport";
import Master from "./pages/Master"
import Dropdown from "./pages/Dropdown1";


function App() {
  const [showNav, setShowNav] = useState(false);
  return (
    <>
      <Router>
        <header>
          <img className="napierimag" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_xi6tP3RBtLL_-sZydneHofsArcKcajGoDQ&usqp=CAU" />
          <GiHamburgerMenu className="hamburger" onClick={() => setShowNav(!showNav)} />
          <Dropdown/>
      
        </header>
        <hr/>
       
        <Navbar show={showNav} />
        <div className="main">
           <Route path="/" exact={true} component={Login} className="center-col"/>
          <Route path="/incidentreport" exact={true} component={IncidentReport} className="center-col"/>
          <Route path="/master" exact={true} component={Master} />
          {/* <Route path="/accordions" exact={true} component={Accordions} className="center-col"/> */}
          
        </div>
      </Router>
  
    </>
  );
}

export default App;
